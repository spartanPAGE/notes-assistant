const annyang = window['annyang'];

export function startListening(): Promise<void> {
  return new Promise<void>(
    (resolve, reject) => {
      if(!annyang) {
        reject('annyang is not available');
      } else {
        
        annyang.start();
      }
    }
  );
}

export function addListeningCommands(commands) {
  annyang.addCommands(commands);
}

export function pauseListening() {
  annyang.pause();
}

export function resumeListening() {
  annyang.resume();
}

export function setListeningLanguage(lang) {
  annyang.setLanguage(lang);
}