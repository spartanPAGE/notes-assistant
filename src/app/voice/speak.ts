function speak(text: string, { voice = "UK English Male", pitch = 0.1, rate = 1, onStart = () => {}, onEnd = () => {} }): void {
  window['responsiveVoice'].speak(text, voice, { pitch, rate, onstart: onStart, onend: onEnd });
}



export class Speaker {
  private static responsiveVoice = window['responsiveVoice'];
  private static queue = [];

  static initialize() {
    setInterval(() => {
      if(!this.isPlaying()) {
        this.apiSpeak();
      }
    }, 10);
  }
  
  public static stop() {
    this.responsiveVoice.cancel();
  }

  public static pause() {
    this.responsiveVoice.pause();
  }

  public static resume() {
    this.responsiveVoice.resume();
  }

  public static isPlaying() {
    return this.responsiveVoice.isPlaying();
  }

  public static speak(text: string, { voice = "UK English Male", pitch = 0.1, rate = 1, onStart = () => {}, onEnd = () => {} } = {}) {
    this.queue.push({
      text,
      data: { voice, pitch, rate, onStart, onEnd }
    });

    if(!this.isPlaying()) {
      this.apiSpeak();
    }
  }

  private static apiSpeak() {
    if(this.queue.length) {
      const phrase = this.queue.shift();
      
      const data = phrase.data;
      
      if(this.queue.length) {
        const onEnd = phrase.data.onEnd;
        data.onEnd = () => {
          onEnd();
          console.log('next speak with: ' + this.queue);
          this.apiSpeak();
        };
      }

      speak(phrase.text, phrase.data);
    }
  }
}

Speaker.initialize();