import { Injectable } from '@angular/core';
import { URLSearchParams, Jsonp } from '@angular/http';

import 'rxjs';

@Injectable()
export class WikiService {
  constructor(private jsonp: Jsonp) {}

  search(term: string, lang: 'pl' | 'en') {
    var search = new URLSearchParams()
    search.set('format', 'json');
    search.set('action', 'query');
    search.set('prop', 'extracts');
    search.set('explaintext', '');
    search.set('titles', term);

    return this.jsonp
      .get(`https://${lang}.wikipedia.org/w/api.php?callback=JSONP_CALLBACK`, { search })
      .map((request) => {
        const obj = request.json()['query']['pages'];
        return obj[Object.keys(obj)[0]]["extract"];
      });
  }
}