import { Injectable } from '@angular/core';
import { URLSearchParams, Http } from '@angular/http';

import 'rxjs';

@Injectable()
export class JokesService {
  constructor(private http: Http) {}

  random() {
    return this.http
      .get('https://api.icndb.com/jokes/random')
      .map((data) => data.json()['value']['joke'].split('&quot;').join(''));
  }
}