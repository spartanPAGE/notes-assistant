import { Speaker } from '../voice/speak';
import { startListening, setListeningLanguage } from '../voice/hear';
import { BehaviorSubject, Observable } from 'rxjs/Rx';

export type Language = 'pl' | 'en-US' | 'en-GB';

export type Voice = 'UK English Male' | 'UK English Female' | 'Polish Female';

export class Assistant {
  private speakStartSubject = new BehaviorSubject(null);
  private speakEndSubject = new BehaviorSubject(null);

  public constructor(
    public language: Language,
    public voice: Voice,
    public pitch: number,
  ) {}

  public activate() {
    console.log('setting up ' + this.language + ' for annyang');
    setListeningLanguage(this.language);
  }

  public speak(phrase) {
    throw 'unused yet';
    /*
    Speaker.speak(phrase, {
      voice: this.voice,
      pitch: this.pitch,
      onStart: () => this.speakStartSubject.next(null),
      onEnd: () => this.speakEndSubject.next(null)
    });
    */
  }

  public get speakStarted(): Observable<void> {
    return this.speakStartSubject.asObservable();
  }

  public get speakEnded(): Observable<void> {
    return this.speakEndSubject.asObservable();
  }
}