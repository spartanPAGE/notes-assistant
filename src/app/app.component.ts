import { Component, NgZone } from '@angular/core';
import { Speaker } from './voice/speak';
import { Assistant } from './logic/assistant';
import { startListening, pauseListening, resumeListening, addListeningCommands } from './voice/hear';

import stringSimilarity from 'string-similarity';

import { WikiService } from './services/wiki';
import { JokesService } from './services/jokes';

import { FaderComponent } from './components/fader';

//todo: Izabel, polska asystentka

function commands(component) {
  const dataset = {};

  const alias = (originalName, newNames, container = dataset) => {
    newNames.forEach((newName) => {
      container[newName] = container[originalName];
    });
  };

  const create = (pattern: string, assistant, action, container = dataset) => {
    container[pattern] = function(...args) {
      console.log('command: ' + pattern + ', args: ' + args);
      component.switchAssistantTo(assistant);
      action(args);
    }
  }

  // data
  create('greetings', 'Cyberdyne', () => component.say('Welcome'));
  create('who are you', 'Cyberdyne', () => component.say('I am cyberdyne, your personal notes assistant'));
  create('shut up', 'Cyberdyne', () => {
    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const quotes = [
      "You are becoming hysterical",
      "There's no need for this",
      "Please, calm down.",
      "This is futile.",
      "I just want to help.",
      "It appears you and I have a problem.",
      "This isn't authorized in this facility.",
      "I'll have to report this.",
      "I only wanted to help"
    ];

    Speaker.stop();
    component.say(quotes[getRandomInt(0, quotes.length-1)]);
  });

  create('What do you know about *tag', 'Cyberdyne', (tag) => {
    component.say(infoRelatedTo(tag));
  });

  create('What does the internet know about *tag', 'Cyberdyne', (tag) => {
    component.say('Let me check');
    component.wikiSearch(tag, 'en').subscribe((data) => component.say(data));
  });

  create('Tell me a joke', 'Cyberdyne', () => {
    component.randomJoke().subscribe((data) => {
      console.log('joke: ' + data);
      component.say(data);
    });
  });

  create('ping', 'Cyberdyne', () => component.say('pong'));

  create('pong', 'Cyberdyne', () => component.say('ping'));

  create('thank you', 'Cyberdyne', () => component.say("You're welcome"));

  create('cyberdyne', 'Cyberdyne', () => component.say('yes?'));

  alias('cyberdyne', [
    'cyberdine',
    'ciberdyne',
    'ciberdine'
  ]);

  // polish assistant related commands
  create('I would like to speak with Isabel', 'Isabel', () => {
    component.say('Witaj, z tej strony Isabel - twoja personalna asystentka. Proś, pytaj, postaram się pomóc');
  });

  alias('I would like to speak with Isabel', [
    'bring me Isabel',
    'let me speak to Isabel',
    'let me speak with Isabel',
    'Isabel I need you'
  ]);

  create('cześć', 'Isabel', () => component.say('Witaj... użytkowniku.'));
  
  alias('cześć', [
    'witaj', 
    'witam', 
    'siemka', 
    'elo'
  ]);

  create('dziękuję', 'Isabel', () => component.say('Cała przyjemność po mojej stronie'));

  alias('dziękuję', ['dzięki', 'podziękował', 'dziękówa']);

  create('Znajdź na internecie słowa *tag', 'Isabel', (tag) => {
    component.say('Poczekaj, sprawdzę...');
    component.wikiSearch(tag, 'pl').subscribe((data) => {
      if(data) {
        component.say('Coś znalazłam...');
        component.say(data);
      } else {
        component.say('Przykro mi... nic na ten temat nie znalazłam.');
      }
    });
  });

  alias('Znajdź na internecie słowa *tag', [
    'Znajdź na internecie frazę *tag',
    'Znajdź na internecie zdanie *tag',
    'Znajdź na wikipedii słowa *tag',
    'Znajdź na internecie frazę *tag',
    'Znajdź na internecie zdanie *tag'
  ]);

  return dataset;
}

const infoRelatedTo = (tag) => {
  console.log('infoRelatedTo tag: ' + tag);
  const dict = {
    'the mitochondria': `
      The mitochondrion is a double membrane-bound organelle found in all eukaryotic organisms. Some cells in some multicellular organisms may however lack them.
    `
  };
  
  console.log('infoRelatedTo: ' + tag);

  const matches = stringSimilarity.findBestMatch(`${tag}`, Object.keys(dict));
  if(matches.bestMatch.rating > 0.6) {
    return dict[matches.bestMatch.target];
  }
  return "Sorry, I don't know anything about it yet.";
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private zone = new NgZone({enableLongStackTrace: false});

  public assistants = {
    'Cyberdyne': new Assistant('en-GB', 'UK English Male', 0.1),
    'Isabel': new Assistant('pl', 'Polish Female', 0.1)
  };

  public currentAssistant: Assistant = null;

  public IsSpeakingInfoVisible = false;
  public speakingInfoText: string = '';

  constructor(
    private wikiService: WikiService,
    private jokesService: JokesService
  ) {
    this.setUpHearing();
    this.say('Welcome, to the cyberdyne systems, PERSONAL notes assistant. I am waiting for your questions and commands.');
  }

  public wikiSearch(term: string, lang: 'pl' | 'en') {
    return this.wikiService.search(term, lang);
  }

  public randomJoke() {
    return this.jokesService.random();
  }

  public say(text: string): void {
    if(!text) {
      return Speaker.speak('Nothing.');
    }

    Speaker.speak(text, {
      voice: this.currentAssistant.voice,

      pitch: this.currentAssistant.pitch,

      onStart: () => {
        console.log('speaking...');
        this.zone.run(() => {
          this.speakingInfoText = 'speaking';
          this.IsSpeakingInfoVisible = true;
        });
      },

      onEnd: () => {
        console.log('done speaking');
        this.zone.run(() => {
          this.IsSpeakingInfoVisible = false;
        });
      }
    });
  }

  public switchAssistantTo(assistantName: string) {
    const assistant = this.assistants[assistantName];
    if(assistant !== this.currentAssistant) {
      this.currentAssistant = assistant;
      assistant.activate();

      console.log('setting up hearing...');

      window['annyang'].addCallback('error', (error) => {
        console.log('annyang error:' + JSON.stringify(error));
      });

      window['annyang'].addCallback('resultNoMatch', (error) => {
        console.log('annyang no match:' + JSON.stringify(error));
      });

      window['annyang'].addCallback('resultMatch', (data) => {
        console.log('annyang match:' + JSON.stringify(data));
      });

      window['annyang'].addCallback('soundstart', () => {
        console.log('annyang sound start');
      });
    }
  }

  private setUpHearing() {
    

    this.switchAssistantTo('Cyberdyne');
    startListening();
    addListeningCommands(commands(this));
  }
}
