import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { FaderComponent } from './components/fader';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WikiService } from './services/wiki';

import { JokesService } from './services/jokes';

import { HttpModule, JsonpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    FaderComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    BrowserAnimationsModule
  ],
  providers: [
    WikiService,
    JokesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
