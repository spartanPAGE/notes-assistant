import { 
    Component, OnChanges, Input, 
    trigger, state, animate, transition, style, NgZone 
} from '@angular/core';

@Component({
  selector : 'anim-fader',
  template: `
    <div [@visibilityChanged]="visibility" >
      <ng-content></ng-content>
    </div>
  `,
  animations: [
    trigger('visibilityChanged', [
      state('shown' , style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('* => *', animate('.2s'))
    ])
  ]
})
export class FaderComponent implements OnChanges {
  @Input() isVisible: boolean = true;
  visibility = 'shown';

  ngOnChanges() {
     this.visibility = this.isVisible ? 'shown' : 'hidden';
  }
}