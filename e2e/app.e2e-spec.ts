import { NotesAssistantPage } from './app.po';

describe('notes-assistant App', () => {
  let page: NotesAssistantPage;

  beforeEach(() => {
    page = new NotesAssistantPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
